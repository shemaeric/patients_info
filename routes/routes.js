module.exports = (app) => {
	const doctors = require('../src/controllers/doctorController.js');
	const handlers = require('../src/middleware/Auth.js');

	//create a new doctor account
	app.post('/doctors', doctors.create);

	//Retrieving all doctors
	app.get('/doctors', doctors.findAll);

	//Retrieve a single doctor 
	app.get('/doctors/:doctorId', doctors.findOne);

	//update a doctor with doctorId
	app.put('/doctors/:doctorId', doctors.update);

	//delete a doctor
	app.delete('/doctors/:doctorId', doctors.delete);

	//login
	app.post('/login', handlers.login);

}