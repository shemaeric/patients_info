let Doctor = require('../models/doctorModel.js');

//creating and saving new doctor 
exports.create = (req,res) => {
	if(!req.body.email || !req.body.password) {
		return res.status(400).send({
			'message' : 'Some values are missing'
		});
	}

	let doctor = new Doctor({

		email : req.body.email,
		doctor_id : req.doctor_id,
		password : req.body.password
	});

	//save to database
	doctor.save()
	.then(data => {
		res.send(data);
	}).catch(err => {
		res.status(500).send({
			'message' : 'Some error occured while creating a doctor'
		});
	});
};

//Retrieve and run all doctors from the database
exports.findAll = (req, res) => {
	Doctor.find()
	.then(doctors => {
		res.send(doctors);
	}).catch(err => {
		res.status(500).send({
			message : err.message || "Some error occured while retrieving all doctors"
		});
	});
};

//Retrive and run a single doctor from the database
exports.findOne = (req, res) => {
	Doctor.findById(req.params.doctorId)

	.then(doctor => {
		if(!doctor){
			return res.status(404).send({
				'message' : 'doctor not found'
			});
		}
		res.send(doctor)
	}).catch(err => {
		if(err.kind == 'ObjectId'){
			return res.status(404).send({
				'message' : 'the doctor not found'
			});
		}
		return res.status(500).send({
			'message' : 'Error retrieving the doctor'
		});
	});
},

//updating the doctor
exports.update = (req, res) => {

	//validate the request
	if(!req.body.email) {
		return status.status(400).send({
			'message' : 'The email could not be empty'
		});

	}

	//find the doctor and then update it
	Doctor.findByIdAndUpdate(req.params.doctorId, {
		doctorId : req.body.doctorId,
		email : req.body.email,
		password : req.body.password
	}, {new : true})
	.then(doctor => {
		if(!doctor){
			return res.status(400).send({'message' : 'doctor not found'});
		}
		res.send(doctor);
	}).catch(err => {
		if(err.kind == 'ObjectId') {
			return res.status(404).send({'message' : 'doctor not found'});
		}
		return res.status(500).send({
			'message' : 'Error updating the doctor information'
		});
	});
};

//deleting the doctor
exports.delete = (req, res) => {
	Doctor.findByIdAndRemove(req.params.doctorId)
	.then(doctor => {
		if(!doctor) {
			return res.status(404).send({
				'message' : 'doctor not found'
			});
		}
		res.send({'message' : 'Doctor deleted successfully'});
	}).catch(err => {
		if(err.kind === 'ObjectId' || err.name ==='Note found') {
			return res.status(404).send({
				'message' : 'doctor not found'
			});
		}
		return res.status(500).send({
			'message' : 'could not delete this doctor'
 		});
	});
};
