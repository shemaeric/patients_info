let mongoose = require('mongoose');
// let uuid = require('uuid');


const DoctorSchema = mongoose.Schema({
	// id : uuid.v4(),
	doctor_id : {type : Number},
	email : {
		type : String,
		trim : true,
		required : true,
		unique : true
	},
	password : {
		type : String,
		trim : true,
		required : true
	}

},{
	timestamps : true
});

module.exports = mongoose.model('Doctor', DoctorSchema);