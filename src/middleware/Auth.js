let jwt = require('jsonwebtoken');
const config = require('./config.js');
const Doctor = require('../models/doctorModel.js')

let checkToken = (req, res, next) => {
	if (token.startsWith('Bearer')) {
		//Remove Bearer from string 
		token = token.slice(7, token.length);
	}

	if (token) {
		jwt.verify(token, config.secrete, (err, decode) => {
			if (err) {
				return res.json({
					success : false,
					message : 'Token is not valid'
				});
			} else {
				req.decode = decoded;
				next();
			}
		});
	} else {
		return res.json({
			success : false,
			message : 'Auth is not supplied'
		});
	}
};

let login = (req, res, next) => {
	let email = req.body.email;
	let password = req.body.password;

	// check for data in the database

	let emailInDb = Doctor.findOne(req.params.email)
    //let passwordInDb = Doctor.findOne();
    console.log(emailInDb)

    if (email && password) {
    	if (email === emailInDb && password === passwordInDb) {
    		let token = jwt.sign({email : email},
    			config.secret,
    			{expiresIn : '24h' // expires in 24 hours
    		}
    		);
    		//return the JWT for the future API calls
    		res.json({
    			success : true,
    			message : 'Authentication successful!',
    			token : token
    		});
    	} else {
    		res.status(403).json({
    			success : false,
    			message : 'Incorrect email or password'
    		});
    	}
    } else {
    	res.send(400).json({
    		success : false,
    		message : 'Authentication failed, Please check the request!'
    	});
    }
}

module.exports = {
	checkToken : checkToken,
	login : login
}